# iSpring Players
This library contains the modified versions of the iSpring presentation, quiz and interactive players and supporting files used by the Doddle iSpring presentation engine. The engines are in the `src/lib/players` folder.

## Presentation Player
This player (`players/presentation.js`) is a modified version of the `res/data/player.js` iSpring player taken from the published presentation content. The various modifications are descibed below.

### Slide handler modifications
Each slide in an iSpring presentation is a fragment of HTML (for more details on slide structure see [presentation-player](../presentation-player/README.md#)). This fragment is contained in the slide code file `slide<slideIndex>.js` where the fragment is loaded by passing it to a handler defined using the UUID of the presentation that slide belongs to. This handler is created on the window scope by the iSpring player at setup time.  
The handler takes the index of the slide and the HTML fragment to use to display it. The handler will create an element to hold the HTML fragment but won't add it to the document until that slide is shown.  
During the presentation load, the handler is created and each slide JavaScript file is executed, which will call the handler to create the slide element for later use.  
The load handler has been modified so that the slide HTML fragment description is transformed by the `transformSlideContent` method before it is stored for use by the player. This method is defined by the `PresentationService` in the `presentation-player` library. The `SlideTransformerService` from the `presentation-player` library handles the transformation.  
Transformations to the HTML fragment are necessary because the asset paths used in the HTML fragment are relative. The fragment will be loaded into the Doddle engine context, but the assets it needs are in the content package location, so the relative URLs need to be transformed to absolute links to the correct locations in the Doddle content directory.

The original slide handler is reproduced below with added documentation comments:

```javascript
/** 
* This method creates the slide handler in the global scope
* and is called for each slide index
* during iSpring presentation player setup
* @param (string) a The presentation UUID identifier
* @param (number) b The slide index to initialise
* @param (function) c The method which will create
* the slide DIV containing the slide content to be added to the document
* when the slide is visible
*/
function FJ(a, b, c) {
    // Initialises the content element creation method for this slide
    // This is called by the slide JavaScript via the handler defined below.
    PJ[b] = c;
    a = "sl_" + a;
    window[a] ||
      // This is the slide handler method.
      // parameters:
      // a: the slide index
      // b: the slide HTML
      // c: optional configuration parameters
      (window[a] = function (a, b, c) {
        // Gets the stored content element creation method
        // which was added above
        var d = PJ[a];
        // Calls the method which creates the slide element
        // to add to the player document when that slide
        // is visible and clears the reference to that method once called.
        d && (d(b, c || null), (PJ[a] = null));
      });
  }
  // The slide element content creation methods 
  // are stored in this array for retrieval later
  var PJ = [];
```

The modified version is reproduced below:

```javascript
  // The functionality of this remains the same,
  // but with the additional step that the input
  // slide HTML is transformed before it is passed
  // to the slide element content creation method
  function FJ(a, b, c) {
    PJ[b] = c;
    a = 'sl_' + a;
    window[a] ||
    (window[a] = function(a, b, c) {
      // Get the transformation method from the global scope
      var d = window['transformSlideContent'];
      // Transform the HTML by passing it to the method if it exists
      b = d ? d(a, b) : b;
      // Continue as before...
      d = PJ[a];
      d && (d(b, c || null), (PJ[a] = null));
    });
  }
  var PJ = [];
```

### Extraction of iScroll dependency
Many of the engines include the iScroll library, the presentation engine included. To reduce code duplication and improve maintainability the iScroll definition has been removed from the Presentation engine. The iScroll library has been moved to the `src/lib/iscroll.js` file and is included once by the top level player.