# iSpring Presentation Engine
Project to build the Doddle engine which displays iSpring presentations. This project uses the `PresentationPlayer` component from the *presentation-player* library (`libs/presentation-player`) to load and manage the presentation.

## Presentation Engine entry point
The `PresentationPlayer` component will load the `assets/presentation.html` entry point to display the iSpring presentation in. In this file the iSpring player is initialised and a handler for the slide change event is created. 

### Boardworks Interactive content
In the slide change hander the new slide content is checked for the Boardworks interactive placeholder element `bw-interactive-placeholder`. Each element found is replace with an iFrame whose src is set to the engine url provided in the `presentationconfiguration` message from the `PresentationPlayer` and the content query parameter is set from the `data-interactive` attribute on the placeholder element. The iFrame dimensions are taken from the `data-width` and `data-height` attributes.

## Building the engine
To build the engine, run the node command:

`npm run build.presentation-engine`

This will build the engine and interactive engine dependencies and zip it for upload to Doddle. The output will be an archive: `dist/apps/PresentationEngine<date>.zip` where `<date>` is the date it was built (formatted as yyyyMMdd).
To add new interactive engine dependencies into the build add their build script call to `build.presentation-engine-dependencies` and copy commands into `add.presentation-engine-dependencies` to copy the dependency output to the correct `/assets/engines` location.