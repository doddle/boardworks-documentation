# Presentation Player

This library contains components for loading and managing iSpring presentations.

## iSpring Presentation Content
The iSpring PowerPoint add-in publishes a presentation as a single package containing the presentation content, the iSpring presentation, quiz and interaction players. The structure of an example package (excluding SCORM xml/xsd) is described in [Original iSpring Presentation Content Structure](#original-ispring-presentation-content-structure).  
On Doddle content resources are stored as separate entities from the code (the engine) used to display them to the user. Keeping the content and engine separate leads to improved maintainability as resolving issues in the engine will affect all content run by that engine. Due to this content/engine separation, the original iSpring presentation content structure needs post-processing to remove the redundant iSpring players from the content resource package. This post-processing is done using the [electron-ispring-post-processor](../../apps/electron-ispring-post-processor) application. The post-processed content structure is descibed in the [Doddle iSpring Presentation Content Structure](#doddle-ispring-presentation-content-structure) section.

### Original iSpring Presentation Content Structure
The published zip archive from the iSpring PowerPoint add-in contains all the content assets and configuration as well as the code necessary to display the presentation to the user.
When published as a SCORM package the content archive will contain the usual SCORM manifest and associated schema definitions.

#### Presentation resource folder
The top level `res` folder contains the presentation content. At a minimum it will contain an `index.html` which is the entry point intended for use by iSpring (although this isn't how they are used on Doddle). The presentation configuration is stored in this `index.html` file. If the presentation is packaged as a SCORM resource there will also be a `lms.js` file which contains the iSpring SCORM interface code.

#### Presentation data subfolder
Within the `res` folder the `data` subfolder contains the presentation specific assets such as fonts, images and cursors. The `player.js` file is the iSpring presentation engine. There will be a `slide<index>.js` and `slide<index>.css` for each slide in the presentation which contains the styling and description for each slide (e.g. `slide1.js`, `slide1.css`). 

#### iSpring Interactions
When iSpring interaction content is used in the presentation, there will be an `intr<index>.js` file (for each interaction) which describes the interaction, an `intplayer<index>.js` for each containing the player used to run the specific type of interaction, and a subfolder `intr<index>` containing any fonts and images used.  

#### iSpring quizzes
Whwn iSpring quiz content is used in the presentation, there will be a `quiz<index>.js` quiz description file, and associated `quiz<index>` folder containing the assets. There will also be a single `quizplayer.js` iSpring quiz player used by all quiz slided. When a quiz is embedded in a presentation, there is no SCORM interaction with the server from the quiz.

#### iSpring presentation configuration
In the original iSpring presentation structure the presentation configuration is stored within the `index.html` as an encoded string. The data is JSON which has been obfuscated using a base64 encoding and character code bit shifting algorithm. When the configuration is loaded at runtime this encoded string is decoded back to JSON and utilised.  
During the post-processing of the original iSpring presentation output for use on Doddle, the encoded presentation configuration is extracted from the `index.html`, decoded and stored as plain JSON (`res/presentation.json`). This decoding is done using the original iSpring decode methods which are included in the `libs/ispring-presentation-decoder` [package](../ispring-presentation-decoder).

### Doddle iSpring Presentation Content Structure
The iSpring generated structure is not a good fit for the Doddle content/engine architecture as the iSpring players are included inside each content package, making maintenance more challenging.
To help the content better align with the Doddle architecture, the packages are run through the *iSpring Post Processor* application ([project](../../apps/electron-ispring-post-processor)) which will remove the iSpring presentation, quiz and interaction engines and make the package compatible with Doddle.
The iSpring presentation, quiz and interaction players are still required to run the iSpring content, so rather than include them in each content package, they have been included inside this *PresentationPlayer* engine thereby having a single point of maintenance. These engines are copied from the [ispring-players library](../ispring-players) during the build.
Due to the single package structure of the iSpring presentation output, some [runtime transformation](#slide-transformation) of the post-processed content is necessary so that it works within the design of Doddle content/engine architecture.  
The Doddle iSpring content structure created by the post-processor is essentially the same as the input, with `res` folder and presentation slides, interactions and assets kept. The only differences are:
- The iSpring presentation, interaction and quiz players are removed.
- The presentation configuration is extracted from the `index.html` to a `res/presentation.json` file and the `index.html` is discarded as the Doddle engine is the entry point.  
- If it exists, the SCORM manifest `imsmanifest.xml` is modified so that the `metadata` node is duplicated and added as a child of the `item` node as well as being a child of the root node. This change is necessary because the iSpring PowerPoint addin places the `metadata` on the root node, but Doddle expects to find the `metadata` on the `item` node. The SCORM schema allows either locations. Doddle uses the metadata to populate attributes of the resource in the database (title, description etc.).

## Loading Presentation Content on Doddle
In the Doddle iSpring Presentation engine, the `PresentationPlayerComponent` Angular component is responsible for loading the presentation content. An overview of the load sequence is shown below.

```mermaid
sequenceDiagram
    participant A as PresentationPlayerComponent
    participant B as iSpring presentation player iFrame
    participant S as PresentationService
    participant T as SlideTransformerService
    
    A->>S: Request presentation configuration
    activate S
    S-->S: Load and transform configuration from content pack [1]
    S-->T: Setup slide transformation method
    S->>A: Provide configuration
    deactivate S
    A-->B: Load iFrame [2]
    
    activate B
    B->>A: Notify when ready [3]
    A->>B: Send presentation configuration [4]
    loop Change slide
        B-->B: Load slide configuration
        B->>T: request slide content transformation [5]
        T->>B: slide configuration transformed [6]
    end
    deactivate B
```
&nbsp;

>**Notes:**  
[1]: The configuration is loaded from the content package and tranformations are applied to update asset and engine paths. See [Configuration Transformation](#configuration-transformation).  
[2]: The content loaded into the iFrame is `assets\presentation.html` from the application using this library.  
[3]: The `presentation.html` page will post a `presentationdomloaded` message to the parent window when the `DOMContentLoaded` event triggers.  
[4]: The parent window (`PresentationPlayerComponent`) will respond to the iFrame `presentationdomloaded` message by posting a `presentationconfiguration` message containing the presentation configuration. The iSpring player in the iFrame will then load this configuration.  
[5]: When a slide changes, the iSpring player loads the JavaScript which describes the slide. This HTML description is send to a load handler where it is transformed and stored. See [Slide Transformation](#slide-transformation)    
[6]: The load handler is defined in the iSpring player, but is modified so that the slide HTML is passed to the PresentationService for it to apply a transform before it is used.

## Presentation Configuration
The presentation configuration is extracted from the `index.html` by the post-processor and decoded and stored in a Doddle iSpring presentation resource as `res/presentation.json`. 

### Configuration properties
The format of the presentation configuration data is not documented by iSpring and the properties are very terse, but a brief overview of what has been discerned about the configuration structure is given below. *Properties whose function is not known are not included*. 
- `i` This is the string identifier (UUID) for the presentation. E.g. "{BC423D86-E70D-4824-8EF6-6C1984CED30A}". This is used to define the slide handler (as `sl_<UUID>`). See [Slide Transformation](#slide-transformation).
- `t`, `ct` The presentation title. One *may* be for the window title similar.
- `w` The presentation width in pixels.
- `h` The presentation height in pixels.
- `c` The id of the element containing access to the iSpring player (E.g. "coreSpr_13233541"). This element will have a method `getCore` added which returns the player instance. Used by slides to interact with the player E.g:
  ```javascript
  var id = 'The id of the element in the config';
  getElementById(id).getCore().gotoNextSlide(this);
  ```
- `ui` This appears to be an identifier for the version of the iSpring Suite used to generate the content.
- `s` This is an array of slides in the presentation.
- `f` Font data for the presentation
- `k` Appears to be style information
- `I` Appears to be key value pairs for use in the presentation for labels etc.

#### Slide properties
Not all these properties are present on every slide entry.
The `s` array in the configuration contains a series of slide configuration data objects. There are several types of slide in a presentation; a presentation slide (the default), a quiz slide or an interaction slide.
- `st` The type of slide the data represents. This will exist on non-presentation slides. A quiz slide will have a value of `"q"` and an iSpring interaction will have a value of `"i"`. 
- `t` The slide title
- `s` The JavaScript file which defines the slide.
- `c` The stylesheet file for the slide.
- `T` The thumbnail data for the slide. `i` is the path to the image, `w` and `h` are the width and height for the image.
- `x` The plaintext of the slide
- `n` The notes plaintext for the slide.
- `N` The notes as HTML.
- `pp` For quiz and interaction slides this is the path to the player to load to display the slide content.
- `it` For interaction slides this is the type of interaction. <a name="interaction-types"></a> Posible values are:
  - `iSpring.Steps`
  - `iSpring.Timeline`
  - `iSpring.CyclicProcess`
  - `iSpring.Process`
  - `iSpring.LabeledGraphic`
  - `iSpring.GuidedImage`
  - `iSpring.HotspotImage`
  - `iSpring.CircleDiagram`
  - `iSpring.Pyramid`
  - `iSpring.Glossary`
  - `iSpring.MediaCards`
  - `iSpring.FAQ`
  - `iSpring.Accordion`
  - `iSpring.Tabs`
  - `iSpring.Steps`
  - `iSpring.HotspotImage`

## Configuration Transformation
The presentation configuration is transformed by the PresentationService so that asset paths match the content location.  
The original configuration generated by the iSpring tools expect the assets to be in the same location as the engine, which is not the case on Doddle.  
The configuration also references the quiz and interaction engines for those slides as relative locations, but those engines are included in the Doddle iSpring engine rather than in the content package. These relative links are updated at runtime to reference the correct files from within the engine location (see `PresentationService#updatePlayer`).
The slide data in the configuration is transformed so that the engines referenced for interaction and quiz engines (`pp` property) point to versions included in the Doddle iSpring engine rather than versions bundled in the original content. For interactions the engine to use is determined by the type of interaction given by the `it` property (see [the interaction types](#interaction-types)), for example, an interaction of type `iSpring.Steps` has the slide engine `pp` value set to the `players/iSpring.Steps.js` engine bundled with the Doddle engine.

## Slide Transformation
The Doddle engine will tranform the slide content as it is loaded and before it is shown by the iSpring presentation engine.

### Why?
The iSpring PowerPoint add-in generated package includes both the engine and the content, so the links to assets are all relative within the package. These relative links will not work in the context of Doddle with the engine/content separation because the slide HTML will be loaded into the engine, so the relative links will apply to the engine rather than the content as was intended.  
To resolve this link mismatch it is necessary for the HTML to be transformed at runtime so that relative links are updated to absolute links pointing to the content package which contains the assets.
Additionally the slide HTML is transformed so that Boardworks (non-iSpring) interactive content can be properly embedded into the slide at runtime. See [Boardworks Interactives](#boardworks-interactives) for more details about these interactives.

### How?
The iSpring presentation engine stores each slide as a separate JavaScript file. These are stored on Doddle in the content pack for a resource as `res/data/slide<slideIndex>.js`. These JavaScript files contain code which passes the HTML representation of the slide to a load handler (the slide handler) defined on the global window scope. An example is shown below (*slide1.js*):

```javascript
(function(){var loadHandler=window['sl_{BBFCCF01-8106-4622-A4F6-1194E8B451A0}'];loadHandler&&loadHandler(0, '<div id="spr0_fc9f4b9"><div id="spr1_fc9f4b9" class="kern slide"><img id="img1_fc9f4b9" src="data/img1.png" width="960px" height="720px" alt="" style="left:0px;top:0px;"/><div id="spr3_fc9f4b9"><img id="img0_fc9f4b9" src="data/img0.png" width="960" height="720" alt="title"/></div><div id="spr4_fc9f4b9" style="left:676.667px;top:698.667px;"><div style="width:0px;"><span id="txt0_fc9f4b9" class="nokern" data-width="139.531250" style="left:75.6px;top:5.477px;">© Boardworks Ltd 2010</span></div></div><div id="spr5_fc9f4b9" style="left:89.167px;top:698.5px;"><div style="width:0px;"><span id="txt1_fc9f4b9" class="nokern" data-width="40.774738" style="left:9.6px;top:5.477px;">1 of 13</span></div></div></div><div id="spr2_fc9f4b9" class="kern slide"><div id="spr6_fc9f4b9" style="left:80px;top:5.667px;"><div style="width:0px;"><span id="txt2_fc9f4b9" class="nokern" data-width="183.695313" style="left:9.6px;top:4.498px;">Drama</span></div><div style="width:0px;"><span id="txt3_fc9f4b9" class="nokern relpos" data-width="360.755463" style="left:9.6px;top:72.547px;">An Inspector Calls</span> <span id="txt4_fc9f4b9" class="nokern relpos" data-width="397.402344" style="left:9.593px;top:72.547px;">: Act One – part one</span></div></div></div></div>');})();
```

The formatted HTML slide content is reproduced below:

```html
<div id="spr0_fc9f4b9">
    <div id="spr1_fc9f4b9" class="kern slide"><img id="img1_fc9f4b9" src="data/img1.png" width="960px" height="720px"
            alt="" style="left:0px;top:0px;" />
        <div id="spr3_fc9f4b9"><img id="img0_fc9f4b9" src="data/img0.png" width="960" height="720" alt="title" /></div>
        <div id="spr4_fc9f4b9" style="left:676.667px;top:698.667px;">
            <div style="width:0px;"><span id="txt0_fc9f4b9" class="nokern" data-width="139.531250"
                    style="left:75.6px;top:5.477px;">© Boardworks Ltd 2010</span></div>
        </div>
        <div id="spr5_fc9f4b9" style="left:89.167px;top:698.5px;">
            <div style="width:0px;"><span id="txt1_fc9f4b9" class="nokern" data-width="40.774738"
                    style="left:9.6px;top:5.477px;">1 of 13</span></div>
        </div>
    </div>
    <div id="spr2_fc9f4b9" class="kern slide">
        <div id="spr6_fc9f4b9" style="left:80px;top:5.667px;">
            <div style="width:0px;"><span id="txt2_fc9f4b9" class="nokern" data-width="183.695313"
                    style="left:9.6px;top:4.498px;">Drama</span></div>
            <div style="width:0px;"><span id="txt3_fc9f4b9" class="nokern relpos" data-width="360.755463"
                    style="left:9.6px;top:72.547px;">An Inspector Calls</span> <span id="txt4_fc9f4b9"
                    class="nokern relpos" data-width="397.402344" style="left:9.593px;top:72.547px;">: Act One – part
                    one</span></div>
        </div>
    </div>
</div>
```

The load handler is retrieved from the window using the UUID which identifies the presentation (in the example *BBFCCF01-8106-4622-A4F6-1194E8B451A0*).  


The `transformSlideContent` method on the global player scope is a reference to the `transformSlideContent` method defined in `PresentationService#getPresentationConfiguration` which is just a wrapper around a call to the `SlideTransformerService#transform` method.

## Boardworks Interactives
By using of the `ispring-presentation-tools` PowerPoint Addin ([repository](https://gitlab.com/doddle/ispring-presentation-tools)) it is possible to embed custom content into an iSpring presentation and display it in the Doddle iSpring Presentation engine. 
The engines used to display this content is supplied in the `presentationconfiguration` message sent to the `assets/presentation.html` entry point of the `presentation-engine` application. The custom interactive engines are bundler within the `presentation-engine` application (at `assets/engines`) during the build process.
The custom content is loaded into the engine within an iFrame on the slide. The iFrame is created in the slide change handler in `presentation.html`. The engine to use is determined by the `type` property on the data supplied with the interactive.
To add new interactive engines an entry should be added to the `PresentationService#engineMap` property containing the relative url (relative to the application) to find the engine. This property is passed in the `presentationconfiguration` message so that the engines can be used at runtime to load the correct content. The keys in the `engineMap` must match the enum values used in the `ispring-presentation-tools` [project](https://gitlab.com/doddle/ispring-presentation-tools) `InteractiveType` Enum so that Editors can select this content/engine type when creating an interactive in an iSpring presentation.