# Content Engine
This is a set of helper classes and Angular Services to make working with Doddle content engines easier. 

## Resource Service
This Angular Service can be used to retreive the URL of the currently running engine, the active content URL and query the user type.
