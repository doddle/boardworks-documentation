# Boardworks

This is a multiple project workspace containing Doddle iSpring content engines and tools as well as the Flash to HTML conversion/engine project.

This project was generated using [Nx](https://nx.dev).

## Projects

### iSpring Presentation Engine
These projects are for the iSpring presentation Doddle engine and accompanying tools. 
It comprises: 
- [presentation-engine](./apps/presentation-engine): The lightweight engine application.

and various the library dependencies:
- [presentation-player](./libs/presentation-player): The components and sevices which load and manage the presentations.
- [ispring-players](./libs/ispring-players): The iSpring players for presentation, quiz and interaction types which are included in the application as dependencies for the content. These are taken from iSpring PowerPoint add-in published content and modified with updates and fixes.
- [content-engine](./libs/content-engine): General services to help manage Doddle content engines.

As well as tools to package the content:
- [electron-ispring-post-processor](./apps/electron-ispring-post-processor): An Electron application which will convert the iSpring PowerPoint published content to a Doddle compatible format. This is just an Electron shell to host the `web-ispring-post-processor` app.
- [web-ispring-post-processor](./apps/web-ispring-post-processor): The actual web application hosted in Electron in the `electron-ispring-post-processor`.
- [ispring-packages](./libs/ispring-packages): Classes used by the post-processor for validating and converting iSpring Quiz/Presentation packages to Doddle compatible content packs.
- [ispring-presentation-decoder](./libs/ispring-presentation-decoder): Classes used by the post-processor to decode the iSpring presentation configuration to plain JSON for inclusion in Doddle content packs.
